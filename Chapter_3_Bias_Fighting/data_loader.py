import pandas as pd

def load_ICU_data(path):
    column_names = ['age', 'workclass', 'fnlwgt', 'education', 'education_num', 
                    'marital_status', 'occupation', 'relationship', 'race', 'sex', 
                    'capital_gain', 'capital_loss', 'hours_per_week', 'country', 'target']
    input_data = (pd.read_csv(path, names=column_names, 
                              na_values="?", sep=r'\s*,\s*', engine='python')
                  .loc[lambda df: df['race'].isin(['White', 'Black'])])

    # sensitive attributes; we identify 'race' and 'sex' as sensitive attributes
    sensitive_attribs = ['race', 'sex']
    Z = (input_data.loc[:, sensitive_attribs]
         .assign(race=lambda df: (df['race'] == 'White').astype(int),
                 sex=lambda df: (df['sex'] == 'Male').astype(int)))

    # targets; 1 when someone makes over 50k , otherwise 0
    y = (input_data['target'] == '>50K').astype(int)

    # features; note that the 'target' and sentive attribute columns are dropped
    X = (input_data
         .drop(columns=['target', 'race', 'sex'])
         .fillna('Unknown')
         .pipe(pd.get_dummies, drop_first=True))
    
    print("features X: ",X.shape[0]," samples, ",X.shape[1]," attributes")
    print("targets y: ",y.shape[0]," samples")
    print("sensitives Z: ",Z.shape[0]," samples, ",Z.shape[1]," attributes")
    return X, y, Z
    
def add_index(X, y, Z):
    # Add an ID to all data point: letter indicative of sex + letter indicative of race + number
    counter = 1
    ids = []
    for i,r in X.iterrows():
        is_male = Z.loc[r.name, 'sex']
        is_white = Z.loc[r.name, 'race']
        letter_sex = 'M' if is_male else 'F'
        letter_race= 'W' if is_white else 'B'
        ids.append(letter_sex+'_'+ letter_race+ '_' + str(counter))
        counter +=1
        
    X['Sample_ID'] = ids
    Z['Sample_ID'] = ids
    
    X = X.set_index('Sample_ID')
    y.index = ids
    Z = Z.set_index('Sample_ID')
    return X, y, Z