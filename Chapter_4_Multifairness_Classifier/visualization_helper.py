import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
sns.set(style="white", palette="muted", color_codes=True, context="talk")
from IPython import display

def plot_distributions(y, Z, iteration=None, val_metrics=None, p_rules=None, fname=None):
    fig, axes = plt.subplots(1, 2, figsize=(10, 4), sharey=True)
    fig.patch.set_facecolor('tab:gray')
    legend={'race': ['black','white'],
            'sex': ['female','male']}
    for idx, attr in enumerate(Z.columns):
        for attr_val in [0, 1]:
            ax = sns.distplot(y[Z[attr] == attr_val], hist=False, 
                              kde_kws={'shade': True,},
                              label='{}'.format(legend[attr][attr_val]), 
                              ax=axes[idx])
        ax.set_xlim(0,1)
        ax.set_ylim(0,7)
        ax.set_yticks([])
        ax.set_title("sensitive attibute: {}".format(attr))
        if idx == 0:
            ax.set_ylabel('prediction distribution')
        ax.set_xlabel(r'$P({{income>50K}}|z_{{{}}})$'.format(attr))
    if iteration:
        fig.text(1.0, 0.9, "Training iteration "+str(iteration), fontsize='16')
    if val_metrics is not None:
        text = "Prediction performance:\n"
        for c, val in val_metrics.iteritems():
            text += "- "+str(c)+": "+ str(val)+ "\n"
        fig.text(1.0, 0.7, text[:-2],fontsize='16')
    if p_rules is not None:
        fig.text(1.0, 0.4, '\n'.join(["Satisfied p%-rules:"] +
                                     ["- "+str(attr)+": "+str(p_rules[attr])+"-rule" 
                                      for attr in p_rules.keys()]), 
                 fontsize='16')
    fig.tight_layout()
    if fname is not None:
        plt.savefig(fname, bbox_inches='tight')
    return fig

def plot_metrics(val_metrics, fairness_metrics):
    fig, axes = plt.subplots(1,2, figsize=(15, 5))
    fig.patch.set_facecolor('tab:gray')
    ax = val_metrics.plot(ylim=(0,100), ax=axes[0])
    ax = fairness_metrics.plot(ylim=(0,100), ax=axes[1])
    fig.tight_layout()
    plt.axhline(y=80, xmin=0, xmax=50, color= 'r')
    plt.show()
    return fig