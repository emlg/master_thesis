import numpy as np
import pandas as pd

from sklearn import linear_model, datasets, feature_extraction, model_selection, ensemble


def p_rule(y, z, threshold=0.5):
    y_1 = y[z == 1]> threshold if threshold else y[z == 1]
    y_0 = y[z == 0]> threshold if threshold else y[z == 0]
    return np.min([y_1.mean()/y_0.mean(), y_0.mean()/y_1.mean()])*100
    
    
# Find the matchings in each subsets
def find_indifferent_matching(X, y, Z, attr):
    # We compute the propensity scores
    # We summarize the unprotected values (note that as we consider Sex here, we also remove race from the columns of X)
    # as the probability to have value Y=1
    logistic = linear_model.LogisticRegression()
    logistic.fit(X= X, y = Z[attr])
    #We predict thanks to the model the probabilities of each person to belong 
    #to each of the 2 labels
    scores = logistic.predict_proba(X = X)
    #To know which column is for 'treat' = 1, we query the classes attribute of the model
    #as the columns of scores are in the same order
    #print(logistic.classes_)
    
    #We create a dictionary that given an id return the propensity score
    propensity = {}
    for i, idx in enumerate(X.index):
        propensity[idx] = scores[i, 1]
        
    # Create a matrix with the Male nodes as rows and Female nodes as columns --> Put 1 so that the diagonal is the max and never taken as the mate
    propensity_difference = np.ones((len(X.index), len(X.index)))
    
    #idx is the range of integers for the lines and columns of the propensity difference matrix
    #id is the letter + number code given to each datapoint
    idx_to_id = {}
    id_to_idx = {}
    
    # Values will be the difference in propensity score 
    for i, m in enumerate(X.index):
        # j is value of column (range(0 to X_F.length)) and f is vlaue in index of X_F dataframe
        for j, f in enumerate(X.index):
            if m != f:
                propensity_difference[i,j] = np.absolute(propensity[m] - propensity[f])
                idx_to_id[i] =m
                id_to_idx[m] = i
            
    
    matching = np.argmin(propensity_difference, axis=0)
    mate = {}
    for col, row in enumerate(matching):
        mate[idx_to_id[col]] = idx_to_id[row]        
        
    return mate, propensity_difference, propensity
    
def indiv_fairness_matching(mate, preds):
    value = 0
    #We sum for all the values
    for idx in preds.index:
        if mate[idx] in preds.index:
            value += np.absolute(preds.loc[idx] - preds.loc[mate[idx]])
        else: 
            print("You should not see this appear")
    
    return value/len(preds)