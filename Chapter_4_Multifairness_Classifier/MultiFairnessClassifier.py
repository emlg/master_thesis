import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from IPython import display
from sklearn.utils.class_weight import compute_class_weight
from sklearn.metrics import accuracy_score, precision_recall_curve, auc
import keras as ke
import keras.backend as K
from keras.layers import Input, Dense, Dropout, Lambda, Concatenate
from keras.models import Model
import tensorflow as tf

from fairness_metrics import *
from visualization_helper import *

class MultiFairnessClassifier(object):
    def __init__(self, n_unprotected, n_protected, lambdas, alphas):
        self.lambdas = lambdas
        self.alphas = alphas
        
        #clf = classifier (all features -> income pred)
        unprotected = Input(shape=(n_unprotected,))
        #adv = adversarial (income pred -> race & sex)
        protected = Input(shape=(n_protected,))
        y_pred = Input(shape=(1,))
        
        #Initialize
        clf_net = self._create_clf_net([unprotected, protected])
        adv_i_net = self._create_adv_i_net(y_pred, n_protected)
        adv_t_net = self._create_adv_t_net([unprotected, y_pred], n_protected)
        self._trainable_clf_net = self._make_trainable(clf_net)
        self._trainable_adv_i_net = self._make_trainable(adv_i_net)
        self._trainable_adv_t_net = self._make_trainable(adv_t_net)
        self._clf = self._compile_clf(clf_net)
        self._clf_w_advs = self._compile_clf_w_advs([unprotected, protected], clf_net, adv_i_net, adv_t_net)
        self._adv_i = self._compile_adv_i([unprotected, protected], clf_net, adv_i_net, n_protected)
        self._adv_t = self._compile_adv_t([unprotected, protected], clf_net, adv_t_net, n_protected)
        self._val_metrics = None
        self._fairness_metrics = None
        
        self.predict = self._clf.predict
        
    def _make_trainable(self, net):
        def make_trainable(flag):
            net.trainable = flag
            for layer in net.layers:
                layer.trainable = flag
        return make_trainable
        
    def _create_clf_net(self, inputs):
        concat_layer = Concatenate()(inputs)
        dense1 = Dense(32, activation='relu')(concat_layer)
        dropout1 = Dropout(0.2)(dense1)
        dense2 = Dense(32, activation='relu')(dropout1)
        dropout2 = Dropout(0.2)(dense2)
        dense3 = Dense(32, activation='relu')(dropout2)
        dropout3 = Dropout(0.2)(dense3)
        #Predict only income level
        outputs = Dense(1, activation='sigmoid', name='y')(dropout3)
        return Model(inputs=inputs, outputs=[outputs])
        
    def _create_adv_i_net(self, y_pred, n_protected):
        dense1 = Dense(32, activation='relu')(y_pred)
        dense2 = Dense(32, activation='relu')(dense1)
        dense3 = Dense(32, activation='relu')(dense2)
        #Predict for the number of sensitive features (Z)
        outputs = [Dense(1, activation='sigmoid')(dense3) for _ in range(n_protected)]
        return Model(inputs=[y_pred], outputs=outputs)

    def _create_adv_t_net(self, inputs, n_protected):
        concat_layer = Concatenate()(inputs)
        dense1 = Dense(32, activation='relu')(concat_layer)
        dense2 = Dense(32, activation='relu')(dense1)
        dense3 = Dense(32, activation='relu')(dense2)
        #Predict for the number of sensitive features (Z)
        outputs = [Dense(1, activation='sigmoid')(dense3) for _ in range(n_protected)]
        return Model(inputs=inputs, outputs=outputs)

    def _compile_clf(self, clf_net):
        clf = clf_net
        self._trainable_clf_net(True)
        clf.compile(loss='binary_crossentropy', optimizer='adam')
        return clf
        
    def _compile_clf_w_advs(self, inputs, clf_net, adv_i_net, adv_t_net):
        clf_w_advs = Model(inputs=inputs, outputs=[clf_net(inputs)]+adv_i_net(clf_net(inputs))+adv_t_net([inputs[0], clf_net(inputs)]))
        self._trainable_clf_net(True)
        self._trainable_adv_i_net(False)
        self._trainable_adv_t_net(False)
        #Weigths to implement Loss_y - lambda * loss_z
        loss_weights = [1.] +[- self.alphas[0]*lambda_param for lambda_param in self.lambdas] +[-self.alphas[1]*lambda_param for lambda_param in self.lambdas] 
        clf_w_advs.compile(loss=['binary_crossentropy']*(len(loss_weights)), 
                          loss_weights=loss_weights,
                          optimizer='adam')
        return clf_w_advs

    def _compile_adv_i(self, inputs, clf_net, adv_i_net, n_protected):
        adv = Model(inputs=inputs, outputs=adv_i_net(clf_net(inputs)))
        #We need to freeze the layers of the classifier net.
        self._trainable_clf_net(False)
        self._trainable_adv_t_net(False)
        #And train only the adversarial
        self._trainable_adv_i_net(True)
        adv.compile(loss=['binary_crossentropy']*n_protected, optimizer='adam')
        return adv
   
    def _compile_adv_t(self, inputs, clf_net, adv_t_net, n_protected):
        adv = Model(inputs=inputs, outputs=adv_t_net([inputs[0], clf_net(inputs)]))
        #We need to freeze the layers of the classifier net.
        self._trainable_clf_net(False)
        self._trainable_adv_i_net(False)
        #And train only the adversarial
        self._trainable_adv_t_net(True)
        adv.compile(loss=['binary_crossentropy']*n_protected, optimizer='adam')
        return adv

    def _compute_class_weights(self, data_set):
        #Function computing the proportion of labels in the given dataset
        class_values = [0, 1]
        class_weights = []
        if len(data_set.shape) == 1:
            balanced_weights = compute_class_weight('balanced', class_values, data_set)
            class_weights.append(dict(zip(class_values, balanced_weights)))
        else:
            n_attr =  data_set.shape[1]
            for attr_idx in range(n_attr):
                balanced_weights = compute_class_weight('balanced', class_values,
                                                        np.array(data_set)[:,attr_idx])
                class_weights.append(dict(zip(class_values, balanced_weights)))
        return class_weights
    
    def _compute_target_class_weights(self, y):
        class_values  = [0,1]
        balanced_weights =  compute_class_weight('balanced', class_values, y)
        class_weights = {'y': dict(zip(class_values, balanced_weights))}
        return class_weights
        
    #--- Usable functions ---
    def light_pretrain(self, x, y, z, epochs=10, verbose=0):
        #Pre-train both networks !
        self._trainable_clf_net(True)
        #Train classifier on entire dataset
        self._clf.fit([x.values, z.values], y.values, epochs=epochs, verbose=verbose)
        
    def pretrain(self, x, y, z, epochs=10, verbose=0):
        #Pre-train both networks !
        self._trainable_clf_net(True)
        #Train classifier on entire dataset
        self._clf.fit([x.values, z.values], y.values, epochs=epochs, verbose=verbose)
        #Freeze classifier
        self._trainable_clf_net(False)
        class_weight_adv = self._compute_class_weights(z)
        #Freeze classifier
        self._trainable_adv_t_net(True)
        class_weight_adv = self._compute_class_weights(z)
        #Train adversarial with the sensitive info and class_weight:
        """class_weight: Optional dictionary mapping class indices (integers) to a weight (float) value, 
        used for weighting the loss function (during training only). 
        This can be useful to tell the model to "pay more attention" to samples from an under-represented class."""
        self._adv_t.fit([x.values, z.values], np.hsplit(z.values, z.shape[1]), class_weight=class_weight_adv, 
                      epochs=epochs, verbose=verbose)
    
    def fit(self, x, y, z, validation_data, matchings, T_iter=250, batch_size=128,
            save_figs=False):
        #Training of the 2 NN in turns.
        n_protected = z.shape[1]
        if validation_data is not None:
            #Just unwrapp input
            x_val, y_val, z_val = validation_data
        test_mate_race, test_mate_sex = matchings
        class_weight_advs = self._compute_class_weights(z)
        class_weight_clf_w_advs = [{0:1., 1:1.}]+class_weight_advs + class_weight_advs
        #Storage of values
        self._val_metrics = pd.DataFrame()
        self._fairness_metrics = pd.DataFrame()  
        for idx in range(T_iter):
            if validation_data is not None:
                #Make the classifier prediction for y
                y_pred = pd.Series(self._clf.predict([x_val, z_val]).ravel(), index=y_val.index)
                #Computed values
                precision, recall, thresholds = precision_recall_curve(y_val, y_pred)
                self._val_metrics.loc[idx, 'PR AUC'] = auc(recall, precision)*100
                self._val_metrics.loc[idx, 'Accuracy'] = (accuracy_score(y_val, (y_pred>0.5))*100)
                display.clear_output(wait=True)
                for sensitive_attr in z_val.columns:
                    self._fairness_metrics.loc[idx, 'Prule_'+sensitive_attr] = p_rule(y_pred, z_val[sensitive_attr])
                
                self._fairness_metrics.loc[idx, 'Indiv_matching_sex'] = 100*indiv_fairness_matching(test_mate_sex, y_pred)
                self._fairness_metrics.loc[idx, 'Indiv_matching_race'] = 100*indiv_fairness_matching(test_mate_race, y_pred)
                
                plot_distributions(y_pred, z_val, idx+1, self._val_metrics.loc[idx],
                                   self._fairness_metrics.loc[idx], 
                                   fname='output/{}.png'.format(idx+1) if save_figs else None)
                plt.show(plt.gcf())
                plot_metrics(self._val_metrics, self._fairness_metrics)
                plt.show(plt.gcf())
            
            # train adverserial
            #Freeze Classifier
            self._trainable_clf_net(False)
            self._trainable_adv_i_net(True)
            self._trainable_adv_t_net(False)
            #Do only one epoch
            self._adv_i.fit([x.values, z.values], np.hsplit(z.values, z.shape[1]), batch_size=batch_size, 
                          class_weight=class_weight_advs, epochs=1, verbose=0)
            self._trainable_clf_net(False)
            self._trainable_adv_i_net(False)
            self._trainable_adv_t_net(True)
            #Do only one epoch
            self._adv_t.fit([x.values, z.values], np.hsplit(z.values, z.shape[1]), batch_size=batch_size, 
                          class_weight=class_weight_advs, epochs=1, verbose=0)
            
            # train classifier
            #Free adversarial
            self._trainable_clf_net(True)
            self._trainable_adv_t_net(False)
            self._trainable_adv_i_net(False)
            #Get a random batch
            indices = np.random.permutation(len(x))[:batch_size]
            self._clf_w_advs.train_on_batch([x.values[indices], z.values[indices]], 
                                           [y.values[indices]]+np.hsplit(z.values[indices], n_protected)+np.hsplit(z.values[indices], n_protected),
                                           class_weight=class_weight_clf_w_advs)
            
        return y_pred, self._val_metrics , self._fairness_metrics