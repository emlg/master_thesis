# Debiasing Classification, a Multifairness Perspective
## EPFL, Data Science Master Thesis
#### Author: Emma LEJAL GLAUDE
##### Professor: Robert West, dlab
##### Supervisor: Claudiu Musat, Swisscom Digital Lab

The content of this repository is the code attached to the mentioned thesis, available as a PDF document in this repository. <br>
The chapter 3 and 4 each have their directory where their respective code is shared.
The data directory contains the public research dataset, UCI Income Census (see reference in the thesis). <br>

The approaches described in chapter 3 are all available in the notebook name Fighting_bias_UCI.ipynb, ensuring that the train-test splitting is maintained. <br>

The chapter 4's folder contains:
- the "data_loader.py" module helping loading the UCI dataset
- the "fairness_metrics.py" module gathering the functions to compute both the Group Fairness metrics and the Individual Fairness metrics.
- the "visualization_helper.py" module that provides the functions to plot the score distributions, the metric evolution and the experiments comparison.
- the "MultiFairnessClassifier.py" module containing the class definition of the novel architecture proposed in the thesis.
- the "Multifairness_classification_exploration.ipynb" notebook that leverages the functions in all the previous module to investigate the bias in the UCI dataset and how it could be debiased. It also contains the function to run multiple experiments and analyze their metrics' averages.
- the "experiments" folder, where the results and data from experiments conducted with the "run_repetitions" method are saved.

Disclosure: the results in the notebooks might not match exactly the thesis' results has they were prettified, commented and tested after the thesis submission date.
